# Transformania Time! Scripts for Metrics Tracking

## Overview
This repository contains scripts for pulling metrics from an MSSQL server with databases for a deployment of Transformania Time!

This is intended for use with Zabbix via an item sourced by a `system.run` key.

To configure connection details for your MSSQL server, copy `creds.py.example` to `creds.py`, run `chown zabbix:zabbix && chmod 660 creds.py` and then add your server connection details.

To pull the most recent turn update time, create an item in Zabbix with the key `system.run["python3 /path/to/your/zabbix-scripts/directory/main.py LastTurnUpdateTime"]`.
To use with a different metrics monitoring suite, you can source data with the command `/path/to/your/zabbix-scripts/directory/main.py LastTurnUpdateTime`. Replace `LastTurnUpdateTime` with one of the metrics from the supported list below. 

You'll likely also have to enable `AllowKey=system.run[*]` in your zabbix_agentd.conf file. 

## Supported Metrics

 - `TotalItems` - The total number of "items" present in the game world.
 - `TotalPlayers` - The total number of "players" present in the game world.
 - `TotalRPClassifiedAds` - The total number of entries in the roleplay classified ads.
 - `TotalRerolls` - The total number of rerolls during the current round.
 - `TotalCovenants` - The total number of covenants that exist in the game world.
 - `TotalPlayerLogs` - The total number of entries in the `PlayerLogs` table, things like "You moved!", "You casted a spell!", etc.
 - `TotalItemTransferLogs` - The total number of entries in the ItemTransferLogs table.
 - `TotalAspNetUsers` - The total number of user accounts.
 - `TotalChatLogs` - The total number of chat messages (this is a rolling 72 hour data set)
 - `TotalMessages` - The total number of direct messages (also a 72 hour rolling window)
 - `TotalBlacklistEntries` - Total number of entries in the blacklist table
 - `TotalStrikes` - Total number of strikes issued to players
 - `TotalCovenantLogs` = Total number of covenant logs
 - `TotalServerLogs` - Total number of server logs
 - `TotalFriends` - Total number of friendship relations
 - `TotalHumanPlayers` - Total number of human players
 - `TotalActiveHumanPlayersLastHour` - Total number human players over the last hour (rolling 1 hour dataset)
 - `PlayerGenderMale` - Number of players with the form gender "male"
 - `PlayerGenderFemale` - Number of players with the form gender "female"
 - `PlayerMobilityAnimal` - Number of animal/pet players 
 - `PlayerMobilityInanimate` - Number of inanimate players 
 - `PlayerMobilityFull` - Number of animate players 
 - `PlayerGameModeSuperProtection` - Number of players with their game mode set to super protection
 - `PlayerGameModeProtection` - Number of players with their game mode set to protection 
 - `PlayerGameModePvP` - Number of players with their game mode set to PvP
 - `BotPsychopathTotal` - Total number of psychopaths that exist, defeated or alive
 - `BotPsychopathDefeated` - Total number of psychoapths that exist in a deafeated state. 
 - `BotPsychopathAlive` - The number of alive psychopaths. Maximum value varies by server configuration.
 - `LastTurnUpdateTime` - The most recent turn update time in milliseconds 
 - `CurrentTurn` - The current turn number 
 - `TotalMessageReports` - Total number of reported messages, can be used with trigger expressions to send notifications
 - `TotalPlayerReports` - Total number of reports that have been made, can be used with trigger expressions to send notifications