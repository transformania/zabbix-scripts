#!/usr/bin/env python3
from creds import *  # Import credentials, if there is a crash here you need to copy `creds.py.example` to `creds.py` and define your database credentials
import argparse  # python argparse
import pymssql  # pymssql, the library we are using to speak to MSSQL

parser = argparse.ArgumentParser(description='Pull metrics from MSSQL')
parser.add_argument('stat', help='https://gitlab.com/transformania/zabbix-scripts')
args = parser.parse_args()

# establish a connection to the MSSQL database
conn = pymssql.connect(db_address, db_user, db_password, db_name)
cursor = conn.cursor()

if (args.stat == "TotalItems"):
    cursor.execute("SELECT count(*) from Items;")
elif (args.stat == "TotalPlayers"):
    cursor.execute("SELECT count(*) from Players;")
elif (args.stat == "TotalRPClassifiedAds"):
    cursor.execute("SELECT count(*) from RPClassifiedAds;")
elif (args.stat == "TotalRerolls"):
    cursor.execute("SELECT count(*) from Rerolls;")
elif (args.stat == "TotalCovenants"):
    cursor.execute("SELECT count(*) from TotalCovenants;")
elif (args.stat == "TotalPlayerLogs"):
    cursor.execute("SELECT count(*) from PlayerLogs;")
elif (args.stat == "TotalItemTransferLogs"):
    cursor.execute("SELECT count(*) from ItemTransferLogs;")
elif (args.stat == "TotalAspNetUsers"):
    cursor.execute("SELECT count(*) from AspNetUsers;")
elif (args.stat == "TotalChatLogs"):
    cursor.execute("SELECT count(*) from ChatLogs;")
elif (args.stat == "TotalMessages"):
    cursor.execute("SELECT count(*) from Messages;")
elif (args.stat == "TotalBlacklistEntries"):
    cursor.execute("SELECT count(*) from BlacklistEntries;")
elif (args.stat == "TotalStrikes"):
    cursor.execute("SELECT count(*) from Strikes;")
elif (args.stat == "TotalCovenantLogs"):
    cursor.execute("SELECT count(*) from CovenantLogs;")
elif (args.stat == "TotalServerLogs"):
    cursor.execute("SELECT count(*) from ServerLogs;")
elif (args.stat == "TotalFriends"):
    cursor.execute("SELECT count(*) from Friends;")
elif (args.stat == "TotalHumanPlayers"):
    cursor.execute("SELECT count(*) from Players WHERE BotId=0;")
elif (args.stat == "TotalActiveHumanPlayersLastHour"):
    cursor.execute(
        "SELECT COUNT(*) FROM Players WHERE BotId = 0 AND OnlineActivityTimestamp > DATEADD(hour, -1, GETDATE());")
elif (args.stat == "PlayerGenderMale"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId = 0 AND Gender='male'")
elif (args.stat == "PlayerGenderFemale"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId = 0 AND Gender='female'")
elif (args.stat == "PlayerMobilityAnimal"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId = 0 AND Mobility='animal'")
elif (args.stat == "PlayerMobilityInanimate"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId = 0 AND Mobility='inanimate'")
elif (args.stat == "PlayerMobilityFull"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId = 0 AND Mobility='full'")
elif (args.stat == "PlayerGameModeSuperProtection"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId = 0 AND GameMode=0")
elif (args.stat == "PlayerGameModeProtection"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId = 0 AND GameMode=1")
elif (args.stat == "PlayerGameModePvP"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId = 0 AND GameMode=2")
elif (args.stat == "BotPsychopathTotal"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId !=0  AND FirstName='Psychopath'")
elif (args.stat == "BotPsychopathDefeated"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId !=0  AND FirstName='Psychopath' AND Mobility!='full'")
elif (args.stat == "BotPsychopathAlive"):
    cursor.execute("SELECT COUNT(*) FROM Players WHERE BotId !=0  AND FirstName='Psychopath' AND Mobility='full'")
elif (args.stat == "LastTurnUpdateTime"):
    cursor.execute(
        "SELECT TOP 1 DATEDIFF(millisecond, Stats.dbo.ServerLogs.StartTimestamp, Stats.dbo.ServerLogs.FinishTimestamp) from Stats.dbo.ServerLogs ORDER BY Stats.dbo.ServerLogs.Id DESC")
elif (args.stat == "CurrentTurn"):
    cursor.execute("SELECT TurnNumber FROM Stats.dbo.PvPWorldStats")
elif (args.stat == "TotalMessageReports"):
    cursor.execute("SELECT COUNT(*) FROM Messages WHERE IsReportedAbusive='true'")
elif (args.stat == "TotalPlayerReports"):
    cursor.execute("SELECT COUNT(*) FROM Reports");
elif (args.stat == "LastCreatedAccount"):
    cursor.execute("SELECT TOP 1 UserName FROM dbo.AspNetUsers WHERE Approved IS NULL ORDER BY CreateDate DESC");
elif (args.stat == "LindellaBotItemStock"):
    cursor.execute(
        "SELECT COUNT(Items.Id) FROM dbo.Items LEFT JOIN dbo.Players AS ItemPlayers ON Items.FormerPlayerId = ItemPlayers.Id LEFT JOIN dbo.Players AS OwnerPlayers ON Items.OwnerId = OwnerPlayers.Id WHERE OwnerPlayers.BotId = -3 AND ItemPlayers.BotId = -2");
elif (args.stat == "LindellaItemStock"):
    cursor.execute(
        "SELECT COUNT(Items.Id) FROM dbo.Items LEFT JOIN dbo.Players AS OwnerPlayers ON Items.OwnerId = OwnerPlayers.Id WHERE OwnerPlayers.BotId = -3");
elif (args.stat == "LindellaSentientStock"):
    cursor.execute(
        "SELECT COUNT(Items.Id) FROM dbo.Items LEFT JOIN dbo.Players AS ItemPlayers ON Items.FormerPlayerId = ItemPlayers.Id LEFT JOIN dbo.Players AS OwnerPlayers ON Items.OwnerId = OwnerPlayers.Id WHERE OwnerPlayers.BotId = -3 AND ItemPlayers.BotId = 0");
elif (args.stat == "LindellaRerolledStock"):
    cursor.execute(
        "SELECT COUNT(Items.Id) FROM dbo.Items LEFT JOIN dbo.Players AS ItemPlayers ON Items.FormerPlayerId = ItemPlayers.Id LEFT JOIN dbo.Players AS OwnerPlayers ON Items.OwnerId = OwnerPlayers.Id WHERE OwnerPlayers.BotId = -3 AND ItemPlayers.BotId = -1")

row = cursor.fetchone()
if row:
    print(row[0])
conn.close()
